#!/bin/bash

# function with_file(){

# }

# function simple(){

# }

read -p "folder name: " x

read -p "files ?: " ans

mkdir $x

if [[ "$ans" == "yes" ]]; then
	read -p "in ?: " inf
	if [[ "$inf" != "NO" ]]; then
		touch $x/$inf
	fi

	read -p "out ?: " ouf
	if [[ "$ouf" != "NO" ]]; then
		touch $x/$ouf
	fi
	cp -v me/fmain.cpp $x/main.cpp
fi

if [[ "$ans" == "no" ]]; then
	cp -v me/main.cpp $x/main.cpp
fi

cp -v me/Makefile $x/Makefile
